#!/bin/bash
mkdir -p ~/projects/stam/L03/unit-test
code ~projects/stam/L03/unit-test/ 

git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

echo "node_modules" > .gitignore

git remote add orginal https://gtilab.com/{Saha112}/unit-test
git remote set-url origin ...
git push -u orginal main

touch README.md
mkdir src test
touch src/main.js scr/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

# test files that goes to stagin area
git add . --dry-run
