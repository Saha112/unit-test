/**
 * Adds tow numbers together.
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a + b;

const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};
/**
 * Divides tow numbers.             
 * @param {number} dividend 
 * @param {number} divisor
 * @returns {number}
 * @throws {Error} 0 division
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed");
    const fraction = dividend / divisor;
    return fraction;
}

const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};

export default { add, subtract, divide, multiply }
